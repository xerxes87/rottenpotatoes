Rails.application.routes.draw do
  get 'reviews/index'

  get 'reviews/new'

  get 'reviews/show'

  get 'reviews/create'

  devise_for :moviegoers
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :movies do
    resources :reviews
  end
  root to: "movies#index"
end
