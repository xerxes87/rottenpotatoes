class ReviewsController < ApplicationController
  before_filter :authenticate_moviegoer!
  def index
    @reviews = Review.all
  end

  def new
    @movie = Movie.find(params[:movie_id])
    @review = @movie.reviews.build
  end

  def show
    id = params[:id]
    @review = Review.find(id)
  end

  def create
    @review = Review.create(review_params)
    @review.moviegoer = current_moviegoer
    @movie  = Movie.find_by_id(params[:movie_id])
    @review.movie = @movie
    
    if @review.save
      flash[:notice] = "Review was successfully saved"
      redirect_to movie_reviews_path
    else
      flash[:notice] = "Review was not saved"
      render 'new'
    end
  end
  def review_params
    params.require(:review).permit(:potatoes, :comments)
  end
  
end
